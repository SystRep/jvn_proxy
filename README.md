Here is the brand new Javanaise 2 release !!!

Thanks to javanaise 2 you have a great system to share objects in java.

   **Demonstration Launch**

Frist unzip and open the project in eclipse,

Then you need to have an instance of JvnCoordImpl running ( run JvnCoordImpl as Java Application )

You can test our implementation by launching one or many of our Java Applications

- Irc.java who's running using javanaise 1

- IrcProxy.java using the great Javanaise 2 !!!

- IrcBurst.java who's a stress test of our javanaise2 implementation.




**How to use it programmer side**
    
Javanaise 2 provide a way to share objects with great level of transparency for programmers.

Every shared objects had to implements InterfaceSentence (Read Write) method.

You had to import our classes into your project.

Example of code using javanaise 2 

    JvnServerImpl js = JvnServerImpl.jvnGetServer();

    InterfaceSentence jo = (InterfaceSentence) JvnProxy.newInstance((Serializable) new Sentence(), "IRC");

    Sentence s = jo;


**More Information :**

We kept the initial project structure, and added following classes ;

For Javanaise 1

- JvnObjectImpl.java implementing the given JvnObject interface

- State.java enumerating different lock states (NL RC W RWC R WC )

- JvnCoordRegisteredObject.java, which implements data structures used by coordinator.

For Javanaise 2

- IrcProxy.java, simplified version of Irc.java.

- InterfaceStentence.java which is the interface of sentence.java needed to use dynamic proxy ( modified sentence.java to implement InterfaceSentence.

- Read/WriteAnnotations.java defining annoted interfaces for Read and Write methods of IrcProxy.

For Burst Testing

- IrcBurst.java using Javanaise 2, is a Forked version of IrcProxy, with high speed (without sleep) random Read/Write of few objects.
