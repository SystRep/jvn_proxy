/***
 * JAVANAISE Implementation
 * JvnServerImpl class
 * Contact: 
 *
 * Authors: 
 */

package jvn;

import java.io.Serializable;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;



public class JvnCoordImpl 	
              extends UnicastRemoteObject 
							implements JvnRemoteCoord{
	

	private HashMap<Integer , JvnCoordRegisteredObject> map_Id_regObj;
	private HashMap<String , JvnObject> map_Name_Object;
	

  /**
  * Default constructor
  * @throws JvnException
  **/
	private JvnCoordImpl() throws Exception {
		System.out.println("constructing COORDINATOR");
		// to be completed

		map_Id_regObj = new HashMap<Integer , JvnCoordRegisteredObject>();
		map_Name_Object = new HashMap<String , JvnObject>();

	}
	
	
	public static void main(String[] args){
		
		try{
			LocateRegistry.createRegistry(5555);
            JvnCoordImpl coordinator = new JvnCoordImpl();
            Naming.rebind("//localhost:5555/COORDINATOR/", coordinator);
            System.out.println("Coordinator Ready!");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

  /**
  *  Allocate a NEW JVN object id (usually allocated to a 
  *  newly created JVN object)
  * @throws java.rmi.RemoteException,JvnException
  **/
  public synchronized int jvnGetObjectId()
  throws java.rmi.RemoteException,jvn.JvnException {
		System.out.println("COORDINATOR : allocating a new object Id");
    // to be completed 
	  int id = 1;
	  while(map_Id_regObj.get(id) != null)id++;
	  return id;
  }
  
  /**
	 * Associate a symbolic name with a JVN object
	 * @param jon : the JVN object name
	 * @param jo  : the JVN object 
	 * @param joi : the JVN object identification
	 * @param js  : the remote reference of the JVNServer
	 * @throws java.rmi.RemoteException,JvnException
	 **/
	public synchronized void jvnRegisterObject(String jon, JvnObject jo, Integer joi, JvnRemoteServer js)
			throws java.rmi.RemoteException,jvn.JvnException{
		
		// Create a new JvnCoordRegisteredObject and add it to the "map_Id_regObj" hashMap
		JvnCoordRegisteredObject jCRO = new JvnCoordRegisteredObject(joi, jon, jo, js);
		map_Id_regObj.put(joi, jCRO);
		
		// Add JvnObject with his associated name in "map_Name_Object" hashMap
		map_Name_Object.put(jon,jo);

	}

	/**
	 * Get the reference of a JVN object managed by a given JVN server 
	 * @param jon : the JVN object name
	 * @param js : the remote reference of the JVNServer
	 * @throws java.rmi.RemoteException,JvnException
	 **/
	public JvnObject jvnLookupObject(String jon, JvnRemoteServer js)
			throws java.rmi.RemoteException,jvn.JvnException{

		// Get the JvnObject reference  associated to name "jon" in "map_Name_Object" hashMap
		JvnObject objet = map_Name_Object.get(jon);

		// Unlock object because the requesting server "js" will create a remote object with "write" lock
		if ( objet != null) { 
			((JvnObjectImpl)objet).jvnUnLock();
		}
		
		// return object reference to the requesting server
		return objet;
	}

	/**
	 * Get a Read lock on a JVN object managed by a given JVN server 
	 * @param joi : the JVN object identification
	 * @param js  : the remote reference of the server
	 * @return the current JVN object state
	 * @throws java.rmi.RemoteException, JvnException
	 **/
	public synchronized Serializable jvnLockRead(int joi, JvnRemoteServer js)
			throws java.rmi.RemoteException, JvnException{

		// Check lock state of JvnObject identified by his Id "joi"
		if ( map_Id_regObj.get(joi).getState() == State.W ) { // If actual state is "write"
			// Get the server having the lock "write"
			ArrayList<JvnRemoteServer> serverAvecLock = map_Id_regObj.get(joi).getServers();
			for(JvnRemoteServer s: serverAvecLock){
				if (s != js) { // If the server "js" requesting readLock isn't the one having writeLock
					map_Id_regObj.get(joi).setAppObject(s.jvnInvalidateWriterForReader(joi));
				}
			}
		}
		// Add the server "js" requesting readLock to the server list for further lock notification
		map_Id_regObj.get(joi).addServer(js);
		
		// Set lock state of JvnObject to "read"
		map_Id_regObj.get(joi).setState(State.R);
		
		// Return the JvnObject to the server "js"
		return map_Id_regObj.get(joi).getAppObject();
	}

/**
* Get a Write lock on a JVN object managed by a given JVN server 
* @param joi : the JVN object identification
* @param js  : the remote reference of the server
* @return the current JVN object state
* @throws java.rmi.RemoteException, JvnException
**/
 public synchronized Serializable jvnLockWrite(int joi, JvnRemoteServer js)
 throws java.rmi.RemoteException, JvnException{

		// Get the server having the lock "read"
	   ArrayList<JvnRemoteServer> serverAvecLock = map_Id_regObj.get(joi).getServers();
	   if ( map_Id_regObj.get(joi).getState() == State.W ) { // If actual state is "write"
		   for(JvnRemoteServer s: serverAvecLock){
			   if (s != js) { // invalidatwriter to the server having write lock
				   map_Id_regObj.get(joi).setAppObject(s.jvnInvalidateWriter(joi));
			   }
		   }
	   }else if ( map_Id_regObj.get(joi).getState() == State.R ) { // If actual state is "read"
		   for(JvnRemoteServer s: serverAvecLock){
			   if (s != js) { // invalidatwriter to servers having read lock
				   s.jvnInvalidateReader(joi);
			   }
		   }
	   }

	   // Set lock state of JvnObject to "read"
	   map_Id_regObj.get(joi).setState(State.W);
	   
	   // Reset the list of servers to "js" server requesting the write lock
	   map_Id_regObj.get(joi).resetServer(js);
	   
	   // Return the JvnObject to the server "js" 
	   return map_Id_regObj.get(joi).getAppObject();
 }

	/**
	* A JVN server terminates
	* @param js  : the remote reference of the server
	* @throws java.rmi.RemoteException, JvnException
	**/
  public void jvnTerminate(JvnRemoteServer js)
	 throws java.rmi.RemoteException, JvnException {
	 // to be completed
  	
  }

}


