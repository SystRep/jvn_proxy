package jvn;

import java.io.Serializable;

public class JvnObjectImpl implements JvnObject {
	
	private State state = State.NL;
	private Serializable objet = null;
	private int id;
	
	
	public JvnObjectImpl(int id, Serializable objet) {
		super();
		this.objet = objet;
		this.id = id;
		this.state = State.W;
	}

	public State jvnGetObjectLock() {
		return state;
	}

	public int getId() {
		return id;
	}
	

	// Acquiring the read lock for the object
	public  void jvnLockRead() throws JvnException {
		if ( state == State.NL ) {
			objet = JvnServerImpl.jvnGetServer().jvnLockRead(id);
			state = State.R;
		}else if (state == State.W || state == State.WC || state == State.RWC) {
			   state = State.RWC;
		}else if (state == State.RC || state == State.R) {
			state = State.R;
		}
	}
	
	// Acquiring the write lock for the object
	public  void jvnLockWrite() throws JvnException {
		if ( state == State.NL || state == State.RC || state == State.R) {
			objet = JvnServerImpl.jvnGetServer().jvnLockWrite(id);
			state = State.W;
		}else if (state == State.W || state == State.WC || state == State.RWC) {
			state = State.W;
		}
	}

	// Unlocking for the object
	public synchronized void jvnUnLock() throws JvnException {
		if (state == State.W  || state == State.RWC) 
			state = State.WC;
		else if (state == State.R ) 
			state = State.RC;
		else if (state == State.RC || state == State.WC) 
			state = State.NL;


		notifyAll();
	}

	public int jvnGetObjectId() throws JvnException {
		return id;
	}

	public Serializable jvnGetObjectState() throws JvnException {
		return objet;
	}

	public synchronized void jvnInvalidateReader() throws JvnException {
		 if (state == State.R ) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			state = State.NL;
		}else {
			state = State.NL;
		}
		
	}

	public synchronized Serializable jvnInvalidateWriter() throws JvnException {
		if (state == State.W ) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			state = State.NL;
		}else if (state == State.WC ) {
			state = State.NL;
		}else {
			state = State.NL; 
		}
		return objet;
	}

	public synchronized Serializable jvnInvalidateWriterForReader() throws JvnException {		
		if (state == State.W ) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			state = State.RC;
		}else if (state == State.WC || state == State.RWC ) {
			state = State.RC;
		}else {
			state = State.NL;
		}
		return objet;
	}


	
}
