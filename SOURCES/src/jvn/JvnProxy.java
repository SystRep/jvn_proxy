package jvn;

import irc.ReadAnnotation;
import irc.WriteAnnotation;

import java.io.Serializable;
import java.lang.invoke.MethodType;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class JvnProxy implements InvocationHandler{
	
	private String name = null; // Not used
	private JvnObject jo = null;
	
	private JvnProxy(JvnObject obj, String n) throws JvnException{
		jo = obj;
		name = n;
	}
	
	//Create a new Proxy instance that allows to share the object obj and register it (if it doesn't exist) in the store with the name name 
	public static Object newInstance(Serializable obj, String name) throws IllegalArgumentException, JvnException {
		
		JvnServerImpl js = JvnServerImpl.jvnGetServer();

		// code taken from IRC.java for creating JVN object.
		// look up the IRC object in the JVN server
		// if not found, create it, and register it in the JVN server

		JvnObject jo = js.jvnLookupObject(name);
		if (jo == null) {
			jo = js.jvnCreateObject(obj);
			
			// after creation, I have a write lock on the object => have to unlock
			jo.jvnUnLock();
			
			js.jvnRegisterObject(name, jo);
		}
		
		return java.lang.reflect.Proxy.newProxyInstance(
				obj.getClass().getClassLoader(),
				obj.getClass().getInterfaces(),
				new JvnProxy(jo, name));
	}
	
	
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		
		if(method.isAnnotationPresent(ReadAnnotation.class)){ // Get Read lock
			System.out.println("read method annotation invokation : ");
			jo.jvnLockRead();
		}else if(method.isAnnotationPresent(WriteAnnotation.class)){ // Get Write lock
			System.out.println("write method annotation invokation");
			jo.jvnLockWrite();
		}else new JvnException("[JvnProxy] error while trying to read the method annotations");
		
		// Call the method
		Object result = method.invoke(jo.jvnGetObjectState(), args);
		// Unlock
		jo.jvnUnLock();
		
		return result;
	}

}
