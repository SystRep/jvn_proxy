package jvn;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;

public class JvnCoordRegisteredObject {
	
	Serializable applicativeObject;
	ArrayList<JvnRemoteServer> lockedServers;
	State lockedServersState;
	
	
	
	public JvnCoordRegisteredObject()
	{
		applicativeObject = null;
		lockedServers = new ArrayList<JvnRemoteServer>();
		lockedServersState = State.NL;
	}
	
	public JvnCoordRegisteredObject(int id,String n, JvnObject jo, JvnRemoteServer js) {
		
		if(this.lockedServers == null) this.lockedServers = new ArrayList<JvnRemoteServer>();
		this.lockedServers.add(js);

		try {
			this.applicativeObject = jo.jvnGetObjectState();
		
			switch(jo.jvnGetObjectLock()){
			case NL:
				lockedServersState = State.NL;
				break;
			case R:
			case RC:
				lockedServersState = State.R;
				break;
			case W:
			case WC:
			case RWC:
				lockedServersState = State.W;
				break;
			}
		} catch (JvnException e) {
			// TODO Auto-generated catch block
			System.out.println("[JvnCoordRegisteredObject] : jvnException : "+e);
		}
	}


	public void setAppObject(Serializable o) {
		this.applicativeObject = o;
	}

	public Serializable getAppObject() {
		return applicativeObject;
	}


	public void setState(State s) {
		this.lockedServersState = s;
	}

	public State getState() {
		return lockedServersState;
	}

	public ArrayList<JvnRemoteServer> getServers() {
		return lockedServers;
	}

	public void addServer(JvnRemoteServer js) {
		this.lockedServers.add(js);
	}

	public void removeServer(JvnRemoteServer js) {
		this.lockedServers.remove(js);
	}

	public void resetServer(JvnRemoteServer js) {
		this.lockedServers.clear();
		this.lockedServers.add(js);
	}

}
