/***
 * JAVANAISE Implementation
 * JvnServerImpl class
 * Contact: 
 *
 * Authors: 
 */

package jvn;

import java.rmi.Naming;
import java.rmi.server.UnicastRemoteObject;
import java.io.*;
import java.util.*;
import java.util.Map.Entry;






public class JvnServerImpl 	
              extends UnicastRemoteObject 
							implements JvnLocalServer, JvnRemoteServer{
	
  // A JVN server is managed as a singleton 
	private static JvnServerImpl js = null;
	
  // Location of rmiregistry
  private String hostName = "//localhost:5555/COORDINATOR/";
	
  // Location of The coordinator in the rmiregistry
  private JvnRemoteCoord coordinator;

  // Table containing <Id , JvnObject> couples
  private HashMap<Integer, JvnObject> objectsTable;
  
  
  /**
  * Default constructor
  * @throws JvnException
  **/
	private JvnServerImpl() throws Exception {
		super();
		System.out.println("SERVER : creating new SERVER");
		// to be completed 

		// Get the coordinator reference
		coordinator = (JvnRemoteCoord) Naming.lookup(hostName);
		
		// Create the objects Table
		objectsTable = new HashMap<Integer, JvnObject>();
	}
	
  /**
    * Static method allowing an application to get a reference to 
    * a JVN server instance
    * @throws JvnException
    **/
	public static JvnServerImpl jvnGetServer() {
		if (js == null){
			try {
				js = new JvnServerImpl();
			} catch (Exception e) {
				return null;
			}
		}
		return js;
	}
	
	/**
	* The JVN service is not used anymore
	* @throws JvnException
	**/
	public  void jvnTerminate()
	throws jvn.JvnException {
		System.out.println("SERVER : terminating");
		// to be completed 
		try {
			coordinator.jvnTerminate(this);
		} catch (Exception e) {
			throw new JvnException("jvnTerminate: "+e);
		}
	} 
	
	/**
	* creation of a JVN object
	* @param o : the JVN object state
	* @throws JvnException
	**/
	public  JvnObject jvnCreateObject(Serializable o)
	throws jvn.JvnException { 
		System.out.println("SERVER : creating new object from serializable = "+o);
		// to be completed 
		int joi = 1;
		JvnObject jo = null;

		try {
			// If we get an id, create a new jvnObject ( with write lock by default) AND
			// create a new entry in the lock and object cache
			joi = coordinator.jvnGetObjectId();
	
	        jo = new JvnObjectImpl(joi, o);
				
			objectsTable.put(joi,  jo);
	
		} catch (Exception e) {
			throw new JvnException("jvnCreateObject: "+e);
		}
		return jo; 
	}
	
	/**
	*  Associate a symbolic name with a JVN object
	* @param jon : the JVN object name
	* @param jo : the JVN object 
	* @throws JvnException
	**/
	public  void jvnRegisterObject(String jon, JvnObject jo)
	throws jvn.JvnException {
		System.out.println("SERVER : registering Object "+jo+" with symbolic name : "+jon);
		// to be completed 
		try {
			// get joi : JvnObject jo Id
			Integer joi = null;
			for(Object Id_Obj : objectsTable.entrySet()){
			    Map.Entry<Integer, JvnObject>entry = (Map.Entry<Integer, JvnObject>) Id_Obj;
				if(entry.getValue() == jo ){
					joi = entry.getKey();
				}
			}
			
			// create the shared object in the coordinator store
			coordinator.jvnRegisterObject(jon, jo, joi, this);
			
		} catch (Exception e) {
			throw new JvnException("jvnRegisterObject: "+e);
		}
	}
	
	/**
	* Provide the reference of a JVN object being given its symbolic name
	* @param jon : the JVN object name
	* @return the JVN object 
	* @throws JvnException
	**/
	public  JvnObject jvnLookupObject(String jon)
	throws jvn.JvnException {
		System.out.println("SERVER : looking up Object with symbolic name "+jon);
		// to be completed 
		try{
			JvnObject tmp = coordinator.jvnLookupObject(jon, this);
	        if (tmp != null) {
	            objectsTable.put(tmp.jvnGetObjectId(), tmp);
	        }
	        return tmp;
		}catch(Exception e){
			throw new JvnException("jvnLookupObject in jvnServerImpl: "+e);
		}
        
	}	
	
	/**
	* Get a Read lock on a JVN object 
	* @param joi : the JVN object identification
	* @return the current JVN object state
	* @throws  JvnException
	**/
   public Serializable jvnLockRead(int joi)
	 throws JvnException {
		System.out.println("SERVER : getting lock READ on object ID : "+ joi);
		// to be completed 
	   
	   try{
			// Ask the coordinator for a read lock and update lock cache
			return coordinator.jvnLockRead(joi, this);
			
		} catch (Exception e) {
			throw new JvnException("jvnLockRead in jvnServerImpl: "+e);
		}

	}	
   
	/**
	* Get a Write lock on a JVN object 
	* @param joi : the JVN object identification
	* @return the current JVN object state
	* @throws  JvnException
	**/
   public Serializable jvnLockWrite(int joi)
	 throws JvnException {
		System.out.println("SERVER : getting lock WRITE on object ID : "+ joi);
		// to be completed 
	   try{
		   // Ask the coordinator for a write lock and update lock cache
		   return coordinator.jvnLockWrite(joi, this);
		   
		} catch (Exception e) {
			throw new JvnException("jvnLockWrite in jvnServerImpl: "+e);
		}
	}	

	
  /**
	* Invalidate the Read lock of the JVN object identified by id 
	* called by the JvnCoord
	* @param joi : the JVN object id
	* @return void
	* @throws java.rmi.RemoteException,JvnException
	**/
  public void jvnInvalidateReader(int joi)
	throws java.rmi.RemoteException,jvn.JvnException {
		System.out.println("SERVER : InvalidateReader on object ID : "+ joi);
		// to be completed 
		// Invalidate the Read lock of the JVN object identified by id
	    objectsTable.get(joi).jvnInvalidateReader();
	    
	};
	    
	/**
	* Invalidate the Write lock of the JVN object identified by id 
	* @param joi : the JVN object id
	* @return the current JVN object state
	* @throws java.rmi.RemoteException,JvnException
	**/
  public Serializable jvnInvalidateWriter(int joi)
	throws java.rmi.RemoteException,jvn.JvnException { 
		System.out.println("SERVER : InvalidateWriter on object ID : "+ joi);
		// to be completed 
		// Invalidate the Write lock of the JVN object identified by id
        return objectsTable.get(joi).jvnInvalidateWriter();
        
	};
	
	/**
	* Reduce the Write lock of the JVN object identified by id 
	* @param joi : the JVN object id
	* @return the current JVN object state
	* @throws java.rmi.RemoteException,JvnException
	**/
   public Serializable jvnInvalidateWriterForReader(int joi)
	 throws java.rmi.RemoteException,jvn.JvnException { 
		System.out.println("SERVER : InvalidateWriterForReader on object ID : "+ joi);
		// to be completed 
		// Reduce the Write lock of the JVN object identified by id 
		return objectsTable.get(joi).jvnInvalidateWriterForReader();
		
	 };

}

 