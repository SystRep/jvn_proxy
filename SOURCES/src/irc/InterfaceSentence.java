package irc;

public interface InterfaceSentence  extends java.io.Serializable{
	
	@WriteAnnotation
	public void write(String text);
	
	@ReadAnnotation
	public String read();

}
