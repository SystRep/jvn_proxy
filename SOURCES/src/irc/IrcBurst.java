/***
 * IrcProxy class : simple implementation of a chat using JAVANAISE
 * Contact: 
 *
 * Authors: 
 */

package irc;

import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.Random;

import jvn.JvnProxy;
import jvn.JvnServerImpl;

public class IrcBurst {
	public TextArea text;
	public TextField data;
	Frame frame;
	InterfaceSentence sentence;

	/**
	 * main method create a JVN object nammed IRC for representing the Chat
	 * application
	 **/
	public static void main(String argv[]) {
		try {

			JvnServerImpl js = JvnServerImpl.jvnGetServer();
			InterfaceSentence jo = (InterfaceSentence) JvnProxy.newInstance((Serializable) new Sentence(), "IRC");
			IrcBurst irc = new IrcBurst(jo);
			irc.burst();

		} catch (Exception e) {
			System.out.println("IRC problem : " + e.getMessage());
		}
	}

	/**
	 * IRC Constructor
	 * 
	 * @param jo
	 *            the JVN object representing the Chat
	 **/
	public IrcBurst(InterfaceSentence jo) {
		sentence = jo;
		frame = new Frame();
		frame.setLayout(new GridLayout(1, 1));
		text = new TextArea(10, 60);
		text.setEditable(false);
		text.setForeground(Color.red);
		frame.add(text);
		data = new TextField(40);
		frame.add(data);
		Button read_button = new Button("read");
		read_button.addActionListener(new readListenerBurst(this));
		frame.add(read_button);
		Button write_button = new Button("write");
		write_button.addActionListener(new writeListenerBurst(this));
		frame.add(write_button);
		frame.setSize(545, 201);
		text.setBackground(Color.black);
		frame.setVisible(true);
	}

	public void burst() throws InterruptedException {
		String tab[] = new String[10];
		String s = null;
		tab[0] = "--a--";
		tab[1] = "--b--";
		tab[2] = "--c--";
		tab[3] = "--d--";
		tab[4] = "--e--";
		tab[5] = "--f--";
		tab[6] = "--g--";
		tab[7] = "--h--";
		tab[8] = "--i--";
		tab[9] = "--j--";
		Random random = new Random();
		int i = 0;
		while (true) {
			if (random.nextBoolean()) {
				s = this.sentence.read();
				this.data.setText(s);
				this.text.append(s + "\n");
			} else {
				i = random.nextInt(9);
				this.sentence.write(tab[i]);
				this.data.setText(tab[i]);
				this.text.append(tab[i] + "\n");

			}
		}

	}
}

/**
 * Internal class to manage user events (read) on the CHAT application
 **/
class readListenerBurst implements ActionListener {
	IrcBurst irc;

	public readListenerBurst(IrcBurst i) {
		irc = i;
	}

	/**
	 * Management of user events
	 **/
	public void actionPerformed(ActionEvent e) {

		String s;
		s = irc.sentence.read();
		irc.data.setText(s);
		irc.text.append(s + "\n");

	}
}

/**
 * Internal class to manage user events (write) on the CHAT application
 **/
class writeListenerBurst implements ActionListener {
	IrcBurst irc;

	public writeListenerBurst(IrcBurst i) {
		irc = i;
	}

	/**
	 * Management of user events
	 **/
	public void actionPerformed(ActionEvent e) {

		String s = irc.data.getText();

		irc.sentence.write(s);
		irc.data.setText(s);
		irc.text.append(s + "\n");

	}
}
